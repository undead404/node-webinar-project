import { DiscogsSearchResponse } from "discojs";
import withPerformance from "./lib/with-performance";

const SPACER_URL =
  "https://img.discogs.com/47e6949f314bff366adba2fecfe62dd2f6ff566d/images/spacer.gif";

export default withPerformance(function ts(
  artistsResponseData: DiscogsSearchResponse
): string {
  const artists = artistsResponseData.results;
  if (artists.length === 0) {
    return SPACER_URL;
  }
  return artists[0].cover_image;
});
