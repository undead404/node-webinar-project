import { config } from "dotenv";
import express, { Request, Response } from "express";
config();
import getPictureUrlWithTs from "./ts";
import pipeImage from "./lib/pipe-image";
import searchArtists from "./lib/search-artists";

const app = express();

app.get("/ts/:artistName", async (request: Request, response: Response) => {
  try {
    const artistsResponseData = await searchArtists(request.params.artistName);
    const pictureUrl = getPictureUrlWithTs(artistsResponseData);
    console.info(pictureUrl);
    pipeImage(response, pictureUrl);
  } catch (error) {
    console.error(error);
    response.status(500);
    response.send(JSON.stringify({ error: `${error.name}: ${error.message}` }));
  }
});
app.listen(3000, () => console.info("Listening on port 3000"));
