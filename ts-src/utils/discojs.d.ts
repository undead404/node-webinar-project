// This is a loose type definition. Expand it if you want.
declare module "discojs" {
  interface ConstructorParams {
    consumerKey: string;
    consumerSecret: string;
  }

  interface DiscogsSearchResult {
    cover_image: string;
  }

  interface DiscogsSearchResponse {
    results: DiscogsSearchResult[];
  }
  interface DiscogsSearchParams {
    query?: string;
    type?: string;
  }
  class Discojs {
    constructor(constructorParams: ConstructorParams);
    searchDatabase(
      searchParams: DiscogsSearchParams
    ): Promise<DiscogsSearchResponse>;
  }
  export default Discojs;

  export { DiscogsSearchResponse, DiscogsSearchParams };
}
