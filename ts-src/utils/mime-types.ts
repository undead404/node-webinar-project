/// <reference path="./mime-types.d.ts" />
import mimeTypes from "mime-types";

export default mimeTypes;
