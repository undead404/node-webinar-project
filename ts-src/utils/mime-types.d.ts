// This is a loose type definition. Expand it if you want.
declare module "mime-types" {
  export function lookup(filePath: string): string | false;
}
