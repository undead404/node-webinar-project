/// <reference path="./discojs.d.ts" />
import Discojs, { DiscogsSearchResponse } from "discojs";

export { DiscogsSearchResponse };
export default Discojs;
