import mimeTypes from "../utils/mime-types";

export default function getMimeTypeByUrl(url: string): string {
  const extension: string | false = mimeTypes.lookup(url);
  if (!extension) {
    throw new Error("Extension not found in image URL");
  }
  return extension;
}
