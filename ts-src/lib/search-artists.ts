import Discojs, { DiscogsSearchResponse } from "../utils/discojs";

const { DISCOGS_CONSUMER_KEY, DISCOGS_CONSUMER_SECRET } = process.env;
if (!DISCOGS_CONSUMER_KEY) {
  throw new Error("Discogs consumer key must be supplied");
}
if (!DISCOGS_CONSUMER_SECRET) {
  throw new Error("Discogs consumer secret must be supplied");
}
const disco = new Discojs({
  consumerKey: DISCOGS_CONSUMER_KEY,
  consumerSecret: DISCOGS_CONSUMER_SECRET,
});

export default function searchArtists(
  artistName: string
): Promise<DiscogsSearchResponse> {
  return disco.searchDatabase({
    query: artistName,
    type: "artist",
  });
}
