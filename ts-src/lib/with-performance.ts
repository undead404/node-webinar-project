import { performance } from "perf_hooks";

const NUMBER_OF_ITERATIONS = 100000000;
export default function withPerformance(f: Function) {
  return function (...args: any[]) {
    const result = f(...args);
    const start = performance.now();
    for (let i = 0; i < NUMBER_OF_ITERATIONS; i++) {
      f(...args);
    }
    console.debug(
      `${f.name}: ${(performance.now() - start) / NUMBER_OF_ITERATIONS}`
    );
    return result;
  };
}
