const dotenv = require("dotenv");
const express = require("express");
const invoke = require("lodash/invoke");
dotenv.config();
const pipeImage = require("./lib/pipe-image");
const searchArtists = require("./lib/search-artists");
const approaches = require("./approaches");

const app = express();

app.get("/:approachName/:artistName", async (request, response) => {
  try {
    const artistsData = await searchArtists(request.params.artistName);
    const pictureUrl = invoke(
      approaches,
      request.params.approachName,
      artistsData
    );
    if (!pictureUrl) {
      response.status(400);
      response.send(JSON.stringify({ error: "Unknown approach" }));
    } else {
      console.info(pictureUrl);
      pipeImage(response, pictureUrl);
    }
  } catch (error) {
    console.error(error);
    response.status(500);
    response.send(JSON.stringify({ error: `${error.name}: ${error.message}` }));
  }
});
app.listen(3000, () => console.info("Listening on port 3000"));
