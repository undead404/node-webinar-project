const { performance } = require("perf_hooks");

const NUMBER_OF_ITERATIONS = 100000000;

module.exports = function withPerformance(f) {
  return function (...args) {
    const result = f(...args);
    const start = performance.now();
    for (let i = 0; i < NUMBER_OF_ITERATIONS; i++) {
      f(...args);
    }
    console.debug(
      `${f.name}: ${(performance.now() - start) / NUMBER_OF_ITERATIONS}`
    );
    return result;
  };
};
