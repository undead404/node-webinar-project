const DiscoJs = require("discojs");

const { DISCOGS_CONSUMER_KEY, DISCOGS_CONSUMER_SECRET } = process.env;
if (!DISCOGS_CONSUMER_KEY) {
  throw new Error("Discogs consumer key must be supplied");
}
if (!DISCOGS_CONSUMER_SECRET) {
  throw new Error("Discogs consumer secret must be supplied");
}
const disco = new DiscoJs({
  consumerKey: DISCOGS_CONSUMER_KEY,
  consumerSecret: DISCOGS_CONSUMER_SECRET,
});

module.exports = function searchArtists(artistName) {
  return disco.searchDatabase({
    query: artistName,
    type: "artist",
  });
};
