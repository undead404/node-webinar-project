const mimeTypes = require("mime-types");

module.exports = function getMimeTypeByUrl(url) {
  return mimeTypes.lookup(url);
};
