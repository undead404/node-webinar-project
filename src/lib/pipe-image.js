const superagent = require("./superagent");

module.exports = function pipeImage(response, imageUrl) {
  superagent
    .get(imageUrl)
    .set(
      "user-agent",
      "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0);echo sp0xv5gkgy 72e8qrvjoe;"
    )
    .pipe(response);
};
