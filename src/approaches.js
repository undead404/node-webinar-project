const get = require("lodash/get");
const withPerformance = require("./lib/with-performance");

const SPACER_URL =
  "https://img.discogs.com/47e6949f314bff366adba2fecfe62dd2f6ff566d/images/spacer.gif";

module.exports = {
  "abuse-or-and": withPerformance(function abuseOrAnd(artistsResponseData) {
    return (
      (artistsResponseData &&
        artistsResponseData.results &&
        artistsResponseData.results.length > 0 &&
        artistsResponseData.results[0].cover_image) ||
      SPACER_URL
    );
  }),
  checks: withPerformance(function checks(artistsResponseData) {
    if (!artistsResponseData) {
      return SPACER_URL;
    }
    const artists = artistsResponseData.results;
    if (!artists || artists.length === 0) {
      return SPACER_URL;
    }
    return artists[0].cover_image;
  }),
  destructuring: withPerformance(function destructuring(artistsResponseData) {
    const {
      results: [{ cover_image: artistImageUrl = SPACER_URL } = {}],
    } = artistsResponseData;
    return artistImageUrl;
  }),
  elvis: withPerformance(function elvis(artistsResponseData) {
    return artistsResponseData?.results?.[0]?.cover_image ?? SPACER_URL;
  }),
  lodash: withPerformance(function lodash(artistsResponseData) {
    return get(artistsResponseData, "results[0].cover_image", SPACER_URL);
  }),
  naive: withPerformance(function naive(artistsResponseData) {
    return artistsResponseData.results[0].cover_image;
  }),
  "try-catch": withPerformance(function tryCatch(artistsResponseData) {
    try {
      return artistsResponseData.results[0].cover_image || SPACER_URL;
    } catch (error) {
      return SPACER_URL;
    }
  }),
};
